package edu.core.java.basic;

public class Cat extends Animal {

    private String surname;

    public Cat(String name, String surname) {
        super(name);
        this.surname = surname;
    }

/*
    @Override
    public int getNameLength() {
        return name.length() + surname.length();
    }
*/

    @Override
    protected void doSound() {
        int i = 0;
        i++;
        // ....


        // super.doSound();
    }
}
