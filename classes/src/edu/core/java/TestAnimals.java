package edu.core.java;

public class TestAnimals {

    public void play() {
        Animal a1 = new Dog();
        Animal a2 = new Cat();


        a1.sound();
        a2.sound();

        if (a1 instanceof Cat) {
            ((Cat) a1).mayow();
        } else {
            a1.sound();
        }

        int ai = Animal.i;
    }
}
