package edu.core.java;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class ListModification {

    public void iteratorExample() {
        final List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(10);

        for (int i = 0; i < list.size(); i++) { // TODO: bad design
            int val = list.get(i);
            if (val > 3) {
                list.add(i, 5);
            }
        }

        for (Integer val : list) { // ConcurrentModificationException
            if (val > 3) {
                list.add(2, 5);
            }
        }

        ListIterator<Integer> iter = list.listIterator();
        while (iter.hasNext()) {
            Integer val = iter.next();
            iter.add(3);
            iter.remove();
        }
    }
}
