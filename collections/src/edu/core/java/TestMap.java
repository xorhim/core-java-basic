package edu.core.java;

import java.util.Map;

public class TestMap {

    private static class Entity {

        private final String id;
        private final int i;

        public Entity(String id, int i) {
            this.id = id;
            this.i = i;
        }

        public String getId() {
            return id;
        }

        public int getI() {
            return i;
        }
    }

    public void testMap() {
        Map<String, Entity> map = null;

        Entity e = new Entity("id1", 1);
        map.put(e.getId(), e);

        if (map.containsKey("id1")) {
            System.out.println("Entity with id1 exists");
        }

        Entity e2 = map.get("id1");
        System.out.println(e == e2); // true

        Entity e3 = map.get("abc");
        System.out.println(e3); // null

        Entity e4 = map.getOrDefault("xyz", new Entity("id0", 0));
        System.out.println(e4.getId()); // id0

        for (String key : map.keySet()) {
            System.out.println(key);
        } // "id1"

        for (Entity entity : map.values()) {
            System.out.println(entity.getI());
        } // 1

        for (Map.Entry<String, Entity> entry : map.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue().getI());
        } // id1: 1
    }
}
