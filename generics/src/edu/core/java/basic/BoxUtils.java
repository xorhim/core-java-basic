package edu.core.java.basic;

public class BoxUtils {

    public void testBox() {
        Box<Integer> bi = new Box<>();
        bi.set(1);

        Box<String> bs = new Box<>();
        bs.set("abcd");

        // Integer i1 = (Integer) bs.get(); // class cast exception

        Pair<String, Double> psd = new Pair<>();
        psd.setLeft("abc");
        psd.setRight(2.3);

        bi.inspect(2.5);
    }

    public void boxInheritance() {
        Box<Number> bn = new Box<>();
        Box<Integer> bi = new Box<>();

        // Box<Number> bn2 = bi; // there is no inheritance relationship
        ImprovedBox<String> ib = new ImprovedBox<>("impBox");
        Box<String> b = ib;
    }
}
