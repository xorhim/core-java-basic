package edu.core.java.basic;

public class MethodUtils {

    public static <K, V> boolean compare(Pair<K, V> p1, Pair<K, V> p2) {
        return p1.getLeft().equals(p2.getLeft()) &&
                p1.getRight().equals(p2.getRight());
    }
}
