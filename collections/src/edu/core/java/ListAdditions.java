package edu.core.java;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class ListAdditions {

    public void testAdditions() {
        List<Integer> ints = Arrays.asList(1, 2, 3, 4, 2, 5);
        int i1 = ints.indexOf(2); // i1 == 1
        int i2 = ints.lastIndexOf(2); // i2 == 4

        Collections.sort(ints);
        Collections.shuffle(ints);

        Random rnd = new Random();
        Collections.shuffle(ints, rnd);

        Collections.reverse(ints);
    }
}
