package edu.core.java;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class BulkOperations {

    public void testBulk() {
        Collection<String> c1 = new ArrayList<>();
        c1.add("abc");
        c1.add("def");

        Collection<String> c11 = new ArrayList<>(c1);

        Collection<String> c2 = new ArrayList<>();
        c2.add("abc");
        c2.add("xyz");
        c2.add("ghi");

        c1.addAll(c2); // "abc", "def", "abc", "xyz", "ghi"
        c11.retainAll(c2); // "abc"

        String[] c1a = c1.toArray(new String[0]);
        List<String> immutableList = Arrays.asList("abc", "def", "ghi");
        immutableList.add("xyz"); // trows exception.
    }
}
