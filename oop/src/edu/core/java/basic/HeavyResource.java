package edu.core.java.basic;

public class HeavyResource implements AutoCloseable {

    public void doSmth() {

    }

    @Override
    public void close() {
        // release heavy resources.
    }
}
