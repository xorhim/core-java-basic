package edu.core.java.basic;

public class ObjectDataTypes {

    public static int si = 1; // static field

    public final int i;
    private double x; // 0
    private char ch; // '\u0000'
    private boolean b; // false
    private String str; // null
    private SimpleDataTypes sdt; // null
    private String s2 = "askjjjdkjakdj";

    public ObjectDataTypes(int i, double x, String str) {
        this.i = i;
        this.x = x;
        this.str = str;
    }

    public ObjectDataTypes(int i, double x, String str, SimpleDataTypes sdt) {
        this(i, x, str);
        this.sdt = sdt;
        this.str = str + s2;
    }

    public void setStr(String str) { // setter
        this.str = str;
    }

    public String getStr() { // getter
        return str;
    }

    public int produceInt(String s1, String s2, int i) {
        // produceInt(String, String, int) -- signature
        return s1.length() + s2.length() + i;
    }

    public int produceInt(String s1, String s2, int i, long l) {
        // produceInt(String, String, int, long) -- signature
        return s1.length() + s2.length() + i;
    }

    public int localVar() {
        int i = 0;
        int i1 = i + 1;

        return i1;
    }

    public void primitiveWrappers() {
        Integer i = 0; // boxing
        Integer i1 = Integer.valueOf(0);
        Boolean b = true; // boxing

        int i2 = i; // unboxing
        int i3 = i.intValue();
    }

    public static String getStaticValue() {
        return si + "";
    }
}
