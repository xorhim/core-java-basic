package edu.core.java.basic;

import java.util.Objects;

public abstract class Animal {

    protected final String name;
    private int l;

    public Animal(String name) {
        this.name = name;
        // l = getNameLength();
    }

    public String getName() {
        return name;
    }

    public final int getTrueNameLenght() {
        return name.length();
    }

/* TODO: bad style
    public int getNameLength() {
        return name.length();
    }
*/

    public void sound() {
        doSound();
    }

    protected abstract void doSound();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Animal animal = (Animal) o;
        return Objects.equals(name, animal.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
