package edu.core.java;

public class Fruit {

    private final String name;
    private int i;

    {
        i = 1;
    }

    public Fruit(String name) {
        this.name = name;
    }

    public Fruit() {
        this("abs");
    }

}
