package edu.core.java.basic;

import java.util.ArrayList;
import java.util.List;

public class WildcardsUtil {

    public int countNonNullObjects(List<?> list) {
        int i = 0;
        for (Object obj : list) {
            if (obj != null) {
                i++;
            }
        }

        return i;
    }

    public int countPopsitiveValues(List<? extends Number> list) {
        int i = 0;
        for (Number num : list) {
            if (num != null && num.doubleValue() > 0) {
                i++;
            }
        }

        return i;
    }

    public void addValue(List<? super Number> list) {
        list.add(1);
        list.add(2.4);
    }

    public void copy(List<? extends Number> in, List<? super Number> out) {
        for (Number num : in) {
            out.add(num);
        }
    }

    public void wildcardsSubTyping() {
        List<? extends Integer> ints = new ArrayList<>();

        List<? extends Number> nums = ints;
    }
}
