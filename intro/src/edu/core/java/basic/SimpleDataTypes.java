package edu.core.java.basic;

public class SimpleDataTypes {

    private void primitiveTypes() {
        byte b = -128; // 8 bits
        short s = 12123; // 16 bits
        int i = 123123123; // 32 bits
        long l = 1029310298; // 64 bits
        float f = - 1.0f; // 32 bits
        double d = 123123.03; // 64 bits
        char c = '\uffff'; // 16 bits Unicode
        char c1 = 'a';
        boolean b1 = false; // 8 bits
        boolean b2 = true;
        boolean b3 = b1 && b2; // b1 || b2 // !b1
        boolean b4 = !b1;
    }

    private void strings() {
        String s = "aewdjwe\u1234";
        s.length();
        char ch = s.charAt(3); // 'd'
        String s1 = "ksdjfhkjsdf";
        String s2 = s + s1;
    }
}
