package edu.core.java.basic;

public class ImprovedBox<T> extends Box<T> {

    private final String name;

    public ImprovedBox(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
