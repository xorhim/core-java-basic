package edu.core.java.basic;

public class EntryPoint {

    private int i;

    private final SomeThing st = new SomeThing() {
        @Override
        public void doSomething() {
            //
        }

        @Override
        public int getSomething() {
            return 0;
        }
    };

    public static void main(String[] args) throws AnimalException {
        new EntryPoint().run();
    }

    public int run() throws AnimalException {
        final int j = 0;
        double x = 0;
        x = j + 1;

        double y = 0;
        final Animal adHocAnimal = new Animal("Tom") {
            @Override
            protected void doSound() {
                System.out.println(i + j + y);
            }
        };

        try {
            new ExceptionsExample().testAnimal(new Cat("Sam", "Jhohnson"));
        } catch (AnimalException e) {
            System.out.println("AnimalException has occurred: " + e.getMessage());
        } catch (SimpleAnimalException e) {
            System.out.println("SimpleAnimalException has occurred: " + e.getMessage());
        } catch (Throwable e) { // Bad style catch all throwable(s)
            System.out.println(e.getMessage());
        } finally {
            // clea-up resources.
        }

        try {
            new ExceptionsExample().testAnimal(new Cat("Sam", "Jhohnson"));
        } finally {
            // do something before leaving method with exception

            return 0; // bad style: exception will be swallowed!!!
        }
    }

    public void testHeadyResource() {
        try (HeavyResource resource = new HeavyResource()) {
            resource.doSmth();
        }
    }
}
