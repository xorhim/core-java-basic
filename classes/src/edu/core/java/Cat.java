package edu.core.java;

import java.util.Date;

public class Cat implements Animal {

    @Override
    public String getName() {
        return "Tim";
    }

    @Override
    public Date getDateOfBirth() {
        return new Date(789);
    }

    @Override
    public void sound() {
        // side-effect
    }

    public void mayow() {
        // side-effect
    }
}
