package edu.core.java;

public class TestColor {

    public void getColorDescr() {
        Color color = Color.BLUE;
        String descr = color.getDescription(); // s = "123"

        String n = color.name(); // name = "BLUE"
        int i = color.ordinal(); // i = 3

        // Enum<Color> -- generic type.
    }
}
