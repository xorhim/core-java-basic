package edu.core.java;

import sun.plugin.services.WIExplorerBrowserService;

public class ControlSatement {

    public void testIf(int i, boolean flag, String s) {
        final double x;
        if (i > 0 && !s.isEmpty() || flag) {
            x = 6;
        } else /*if (i % 2 == 0)*/ {
            x = 5;
        }

        double y = x + 1;
    }

    public void testFor() {
        for (int i = 0; i < 10; ) {
            int j = i + 1;

            i += 2;
            if (i > 5) {
                break;
            }
        }

        String[] strs = {"123", "abv", ""};
        for (String s : strs) { // Iterable
            String str = s + "qwerty";
        }
    }

    public void testWhile() {
        int i = 0;
        while (i < 10) {
            int j = i + 1;
            i += 2;
        }

        int k = 10;
        do {
            int t = k - 1;
            k -= 3;
        } while (k > 0);
    }

    public void breakWhile() {
        int i = 0;
        while (i < 10) {
            if (i % 2 == 1) {
                break;
            }

            i++;
        }
    }

    public void continueWhile() {
        int i = 0;
        while (i < 10) {
            i++;

            if (i % 2 == 1) {
                continue;
            }

            int j = i + 1;
        }
    }

    public void testSwitch(Color color) {
        int i;
        switch (color) {
            case RED:
                color = Color.BLACK;
                i = 2;
                break;

            case BLUE:
                color = Color.WHITE;
                int j = 5;

            case BLACK:
                i = 3;
                break;

            case WHITE:
                i = 4;
                break;

            default:
                i = 5;
        }
    }
}
