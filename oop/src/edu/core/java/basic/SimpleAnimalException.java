package edu.core.java.basic;

public class SimpleAnimalException extends RuntimeException {

    public SimpleAnimalException(String message) {
        super(message);
    }
}
