package edu.core.java.basic;

public class AnimalException extends Exception {

    public AnimalException(String message) {
        super(message);
    }
}
