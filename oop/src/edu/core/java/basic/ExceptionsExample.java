package edu.core.java.basic;

// checked and unchecked
public class ExceptionsExample {

    public void testAnimal(Animal animal) throws AnimalException {
        if (animal.getName().length() < 5) {
            throw new AnimalException("Cannot test animal with such name");
        }

        if (animal.getTrueNameLenght() > 10) {
            throw new SimpleAnimalException("Cannot test animal with such name");
        }
    }
}
