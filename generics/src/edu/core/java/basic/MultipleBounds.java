package edu.core.java.basic;

public class MultipleBounds {

    static class A { /* ... */ }
    interface B { /* ... */ }
    interface C { /* ... */ }

    static class D <T extends A & B & C> { /* ... */ }
}
