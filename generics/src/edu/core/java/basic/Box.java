package edu.core.java.basic;

public class Box<T> implements Comparable<Box<T>> {
    // T stands for "Type"
    private T t;

    public void set(T t) {
        this.t = t;
    }

    public T get() {
        return t;
    }

    public <U extends Number> void inspect(U u) {
        System.out.println("T: " + t.getClass().getName());
        System.out.println("U: " + u.getClass().getName());

    }

    @Override
    public int compareTo(Box<T> o) {
        return 0; // TODO: define linear order on boxes
    }
}
