package edu.core.java;

public enum Color {

    BLACK("This is a black color"),

    WHITE("abc"),

    RED(""),

    BLUE("123"),

    YELLOW("wer"),

    PURPLE("qweqw");

    private final String description;

    Color(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
