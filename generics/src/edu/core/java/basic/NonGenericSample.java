package edu.core.java.basic;

import java.util.ArrayList;
import java.util.List;

public class NonGenericSample {

    public void testGeneralList() {
        List list = new ArrayList();
        list.add(1);
        list.add(2.5);
        list.add("abcde");

        int i = (int) list.get(0);
        String s = (String) list.get(2);
        String s2 = (String) list.get(1);
    }
}
