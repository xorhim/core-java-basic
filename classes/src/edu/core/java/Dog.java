package edu.core.java;

import java.util.Date;

public class Dog implements Animal {

    @Override
    public String getName() {
        return "Ben";
    }

    @Override
    public Date getDateOfBirth() {
        return new Date();
    }

    @Override
    public void sound() {
        // side-effect
    }
}
