package edu.core.java.utils;

import edu.core.java.basic.ObjectDataTypes;

public class Utils {

    private ObjectDataTypes obt;

    public void testStatic() {
        int i = ObjectDataTypes.si;
        String s = ObjectDataTypes.getStaticValue();
    }

    public void testObjectCreation() {
        ObjectDataTypes odt = new ObjectDataTypes(12, 1.5d, "abc");
    }
}
