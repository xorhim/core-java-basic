package edu.core.java;

import java.util.Date;

public interface Animal {

    int i = 0;

    String getName();

    Date getDateOfBirth();

    void sound();
}