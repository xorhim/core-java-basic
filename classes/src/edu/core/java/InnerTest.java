package edu.core.java;

public class InnerTest {

    private final String s;

    public InnerTest(String s) {
        this.s = s;
    }

    public void createInternal() {
        Internal i = new Internal();
    }

    public class Internal {

        public Internal() {
        }

        public String getS() {
            return s;
        }
    }

    public static class StaticInternal {

    }
}
